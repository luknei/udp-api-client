import {createSocket, Socket} from "dgram";
import {setInterval} from "timers";
import {AddressInfo} from "net";
import {AES, enc} from "crypto-js";

interface Signer {
    (msg: string): string;
}

interface Logger {
    log(level: string, message: string): void
}

const timestamp = () => Math.floor(Date.now() / 1000).toString();
const noopLogger = {
    log: (level: string, message: string) => {}
};

export class UdpApiClient {
    protected serverErrByCode: {[key:string]: string} = {
        SAF: 'Invalid Safety Key',
        NOA: 'Useless operation',
        ERR: 'Unknown error',
    };

    constructor(
        public secret: string,
        public sign: Signer = timestamp,
        public timeoutDuration = 5000,
        public retryCount: number = 5,
        public logger?: Logger
    ) {
        this.logger = logger || noopLogger;
    }

    updateDb(uid: string, type: number, port: number, address: string) {
        const msg = `UPDATEDB ${uid} ${type}`;

        return this.sendMessage(msg, port, address);
    }

    reserve(uid: string, durationMinutes: number, type: number, port: number, address: string) {
        const msg = `FIX_UID_ ${uid} ${durationMinutes} ${type}`;

        return this.sendMessage(msg, port, address);
    }
    stopReservation(uid: string, durationMinutes: number, type: number, port: number, address: string) {
        const msg = `FIX_UID_ ${uid} ${durationMinutes} ${type}`;

        return this.sendMessage(msg, port, address);
    }
    startCharging(id: string, port: number, address: string) {
        const msg = `OPEN_ID_ ${id}`;

        return this.sendMessage(msg, port, address);
    }

    stopCharging(id: string, port: number, address: string) {
        const msg = `CLOSE_ID ${id}`;

        return this.sendMessage(msg, port, address);
    }

    protected encrypt(message: string): string {
        return AES.encrypt(message, this.secret)
            .toString();
    }

    protected decrypt(message: string): string {
        return AES
            .decrypt(message, this.secret)
            .toString(enc.Utf8);
    }

    sendMessage(msg: string, port: number, address: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const client = createSocket('udp4');
            const signature = this.sign(msg);
            const message = this.encrypt(`${msg} ${signature}`);

            const rejectClose = (err: Error) => {
                client.close();
                reject(err);
            };
            const intervalHandle = setInterval((state: any) => {
                if (state.done) {
                    return;
                }

                if (++state.attempt > this.retryCount) {
                    state.done = true;

                    return rejectClose(new Error(`API did not respond after ${this.retryCount} retries`));
                }

                client.send(message, port, address, (err: Error) => err && rejectClose(err));
            }, this.timeoutDuration, {attempt: 0, done: false});

            client.once('error', rejectClose);
            client.once('close', () => intervalHandle.unref());
            client.once('message',(msg: Buffer, {port, address}: AddressInfo) => {
                const res = this.decrypt(msg.toString());
                const [ns, sig, code] = res.split(' ');

                this.logger.log('INFO', `S2C ${res} ${address}:${port}`);

                if (code !== 'OK'){
                    const errMsg = this.serverErrByCode[code] || 'Unknown server error returned';

                    return rejectClose(new Error(`${errMsg}`));
                }

                const ackMsg = `ACK ${sig}`;
                const req = this.encrypt(ackMsg);
                this.logger.log('INFO', `C2S ${msg} ${address}:${port}`);
                client.send(req, port, address, (err: Error) => {
                    client.close(() => {
                        if (!err) {
                            resolve(res);
                        } else {
                            reject(new Error('Failed to send ACK msg to the server'));
                        }
                    });
                });
            });

            this.logger.log('INFO', `C2S ${msg} ${address}:${port}`);

            client.send(message, port, address, (err: Error) => err && rejectClose(err));
        });
    }
}
